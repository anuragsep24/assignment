# Assignment

## Features

#### `File handler component` is configurable for 
* isMultiple - For multiple file to upload, default `true`
* headerInfo - Label for upload input, default `''`
* fileExt  - Allowed file extensions, default `JPG,JPEG,MP3,MP4, GIF, CSV, PNG,TXT,ZIP,RAR,ICO`
* maxFiles - Maximum number of files at a time can be uploaded, default `5`
* maxSize - Maximum file size allowed, default `5 MB`
* uploaded - outPut event when file has been uploaded successfully.

#### `CSV file reader urility` is capable for
* Reading a supplied `csv file`.
* Returns the content in form of `rows, columns, worksheet`.

#### `customer manage component` is capable for
* Receive the uploaded file
* Get the file converted in to customer data.
* Display the data in table If data is correct.
* Able to filter the results based on minimal issue count for currect data set.
* Display incorrect data rows in saperate table to notify user about wrong data upload.

#### `customer data utility` is having functionality for
* Reading the CSV file worksheet and convert into customer data.
* Separates  valid and invalid customers list based on the validation function.

## Install
* After cloning the application, run `npm install` .
* Run `npm run start` to run the application.



## Build

* Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

* Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).
* Test coverage is around 90%.

## Running end-to-end tests

* Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


