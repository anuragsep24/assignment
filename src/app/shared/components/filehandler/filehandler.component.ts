import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-file-handler',
  styleUrls: ['./filehandler.component.scss'],
  templateUrl: './filehandler.component.html',
})
export class FileHandlerComponent {
  @Input() public isMultiple = true;
  @Input() public headerInfo: string;
  @Input() public fileExt = 'JPG,JPEG,MP3,MP4, GIF, CSV, PNG,TXT,ZIP,RAR,ICO';
  @Input() private maxFiles = 5;
  @Input() private maxSize = 5; // 5MB
  @Output() private uploaded = new EventEmitter<Array<File>>();
  public onSuccess = false;
  private files: Array<File> = [];

  public errors: Array<string> = [];

  constructor() {}

  private isValidFiles(files: Array<File>) {
    // Check Number of files
    if (files.length > this.maxFiles) {
      this.errors.push(
        `Error: At a time you can upload only ${this.maxFiles}  files`,
      );
      return false;
    }
    return this.isValidFileExtension(files);
  }

  private isValidFileExtension(files: Array<File>): boolean {
    // Make array of file extensions
    const extensions = this.fileExt
      .split(',')
      .map((x) => x.toLocaleUpperCase().trim());
    let isValidCount = 0;
    files.forEach((file: File) => {
      const ext = file.name.toUpperCase().split('.').pop() || file.name;
      if (extensions.includes(ext) && this.isValidFileSize(file)) {
        isValidCount++;
      } else {
        this.errors.push('Error (Extension): ' + file.name);
      }
    });
    return isValidCount === this.files.length;
  }
  private isValidFileSize(file: File): boolean {
    const fileSizeinMB = file.size / (1024 * 1000);
    const size = Math.round(fileSizeinMB * 100) / 100; // convert upto 2 decimal place
    if (size > this.maxSize) {
      this.errors.push(
        `Error (File Size): ${file.name} : exceed file size limit of ${this.maxSize} MB (${size}MB)`,
      );
      return false;
    }
    return true;
  }

  public onFileChange(event: any) {
    this.files = [];
    this.errors = [];
    event.preventDefault();
    for (const file of event.target.files) {
      this.files.push(file);
    }
    if (this.files.length > 0 && this.isValidFiles(this.files)) {
      this.onSuccess = true;
      this.uploaded.emit(this.files);
    }
  }
}
