import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FileHandlerComponent } from './filehandler.component';

describe('FileHandlerComponent', () => {
  let component: FileHandlerComponent;
  let fixture: ComponentFixture<FileHandlerComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileHandlerComponent],
    }).compileComponents();
    fixture = TestBed.createComponent(FileHandlerComponent);
    component = fixture.componentInstance;
  }));

  it('should create the FileHandlerComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should call onFileChange when event change when file type is valid', () => {
    const list = new DataTransfer();
    const file = new File(['content'], 'filename.csv');
    list.items.add(file);
    const fileInputEle = fixture.nativeElement.querySelector('#fileupload');
    fileInputEle.files = list.files;
    fileInputEle.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    expect(component.onSuccess).toBeTruthy();
  });

  it('should call onFileChange when event change when file type is invalid', () => {
    const list = new DataTransfer();
    const file = new File(['content'], 'filename.vln');
    list.items.add(file);
    const fileInputEle = fixture.nativeElement.querySelector('#fileupload');
    fileInputEle.files = list.files;
    fileInputEle.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    expect(component.onSuccess).toBeFalsy();
  });
});
