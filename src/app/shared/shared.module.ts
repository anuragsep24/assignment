import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FileHandlerComponent } from './components';

@NgModule({
  declarations: [
    FileHandlerComponent,
  ],
  exports: [FileHandlerComponent],
  imports: [
    BrowserModule,
  ],
})
export class SharedModule { }
