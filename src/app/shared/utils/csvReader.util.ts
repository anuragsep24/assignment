import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IWorkBook, IWorkSheet, read } from 'ts-xlsx';

export class CsvData {
  constructor(
    public columns: number,
    public rows: number,
    public contentArray: any,
  ) { }
}

export class CsvReader {
  constructor() {}

  private getCsvFileSheet(file: File): Observable<IWorkSheet> {
    return new Observable<any>((observer) => {
      const reader: FileReader = new FileReader();
      reader.onload = (e) => {
        observer.next((e.target as any).result);
      };
      reader.readAsBinaryString(file);
      return () => {
        reader.abort();
      };
    }).pipe(
      map((value: string) => {
        return read(value, { type: 'binary' });
      }),
      map((wb: IWorkBook) => {
        return wb.SheetNames.map((sheetName: string) => {
          const sheet: IWorkSheet = wb.Sheets[sheetName];
          return sheet;
        });
      }),
    );
  }

  private getNoOfCol(worksheet: IWorkSheet): number {
    const ref: string = worksheet['!ref'];
    const fCol: string = ref.split(':')[0];
    const lCol: string = ref.split(':')[1];
    return lCol.charCodeAt(0) - fCol.charCodeAt(0) + 1;
  }
  private getNoOfRows(worksheet: IWorkSheet): number {
    const ref: string = worksheet['!ref'];
    const lCol: string = ref.split(':')[1];
    return parseInt(lCol.substr(1), 10);
  }

  public readFile(file: File): Observable<CsvData> {
    return this.getCsvFileSheet(file).pipe(map((data): CsvData => {
        const content: any = JSON.stringify(data[0]);
        const contentArray: IWorkSheet = JSON.parse(content);
        Object.keys(contentArray).map((key, index) => {
          if (contentArray[key].v === 0) {
            delete contentArray[key];
          }
        });
        return {
          columns: this.getNoOfCol(contentArray),
          contentArray: {...contentArray},
          rows: this.getNoOfRows(contentArray),
        } as CsvData;
      }),
    );
  }
}
