import { CsvData, CsvReader } from './csvReader.util';

describe('Service: Auth', () => {

    let csvReader: CsvReader;
    let csvFile: File;
    beforeEach(() => {
        csvReader = new CsvReader();
        const SimpsonsCSV = `Name,Phone Number,Email
        Homer Simpson,5551234422,homer@springfield.com
        Seymour Skinner,1235663322,a@b.c
        Bart Simpson,2675465026,bart@spring.field
        Montgomery Burns,2233459922,hi@bye.cya
        Mayor Quimby,2222222222,mayor@springfield.gov
        Waylon Smithers,3333333333,ok@hey.bye
        Barney Gumble,111111111111,barney@gumble.gum
        Marge Simpson,2627338461,marge@springfield.com
        Edna Krabappel,2656898220,a@b.c
        Lisa Simpson,2222222222,lisa@bix.com
        Maggie Simpson,2716017739,maggie@spring.field
        Linel Hutz,2745577499,hire@now.me
        Troy McClure,2314928822,troy@acting.now
        Rainer Wolfcastle,2221114455,rainer@acting.now
        Krusty Clown,2321221188,krusty@acting.now,,`;

        csvFile = new File([SimpsonsCSV], 'simpsons.csv', { type: 'text/csv' });

    });

    it(`should accept csv file and read it`, () => {
        csvReader.readFile(csvFile).subscribe((data: CsvData) => {
            expect(data.columns).toBe(4);
            expect(data.rows).toBe(16);
        });
      });

    afterEach(() => {
        csvReader = null;
    });
});
