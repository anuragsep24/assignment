import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CustomerModule } from './customer/customer.module';

@NgModule({
  declarations: [
  ],
  exports: [CustomerModule],
  imports: [
    BrowserModule,
    CustomerModule,
  ],
  providers: [],
})
export class FeaturesModule { }
