import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { CustomerManageComponent } from './manage.component';

describe('CustomerManageComponent', () => {
  let component: CustomerManageComponent;
  let fixture: ComponentFixture<CustomerManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CustomerManageComponent,
      ],
      imports: [
        SharedModule,
        FormsModule,
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(CustomerManageComponent);
    component = fixture.componentInstance;
  }));

  it('should create the CustomerManageComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should call getUploadedFiles and set the data in right table when data is valid', () => {
    const issueCSV = `First Name,Last Name,Issue Count,Date of Birth
    John,Doe,3,09-12-2001
    David,Garner,5,09-12-1989
    Jolly,Doe,3,09-12-1987
    jipsom,Doe,3,09-12-1987
    `;
    const fileList = [new File([issueCSV], 'issueCSV.csv', { type: 'text/csv' })];
    component.getUploadedFile(fileList);
    fixture.detectChanges();
    expect(component.invalidCustomerList.length).toBe(0);
  });

  it('should call getUploadedFiles and set the data in Error table when data is invalid', () => {
    const issueCSV = `First Name,Last Name,Issue Count,Date of Birth
    John,Doe,3,12-2001
    David,Garner,5,12-1989
    Jolly,Doe,3,12-1987
    jipsom,Doe,3,12-1987
    `;
    const fileList = [new File([issueCSV], 'issueCSV.csv', { type: 'text/csv' })];
    component.getUploadedFile(fileList);
    fixture.detectChanges();
    expect(component.customerList.length).toBe(0);
  });
});
