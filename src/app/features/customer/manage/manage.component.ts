import { Component } from '@angular/core';
import { merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CsvData, CsvReader } from '../../../shared/utils';
import { csvToCustomerList, Customer, CustomerViewData } from '../customer.util';

@Component({
  selector: 'app-customer-manage',
  styleUrls: ['./manage.component.scss'],
  templateUrl: './manage.component.html',
})
export class CustomerManageComponent {
  private csvReader: CsvReader;
  public customerList: Array<Customer> = [];
  public invalidCustomerList: Array<Customer> = [];
  public searchCount: number;

  constructor() {
    this.csvReader = new CsvReader();
  }

  public getUploadedFile(files: Array<File>) {
    this.customerList = [];
    this.invalidCustomerList = [];
    merge(...files.map((file: File): Observable<CsvData> => {
      return this.csvReader.readFile(file);
    })).pipe(
      map((csvData: CsvData): CustomerViewData => {
        return csvToCustomerList(csvData);
      }),
    ).subscribe((data: CustomerViewData) => {
      this.customerList = this.customerList.concat(data.customerList);
      this.invalidCustomerList =  this.invalidCustomerList.concat(data.invalidCustomerList);
    });
  }
}
