import { CsvData } from 'src/app/shared/utils';

export class Customer {
  constructor(
    public firstName: string,
    public lastName: string,
    public issueCount: number,
    public dateOfBirth: string,
  ) {}
}

export class CustomerViewData {
  constructor(
    public customerList: Array<Customer>,
    public invalidCustomerList: Array<Customer>,
  ) {}
}

export const csvToCustomerList = (csvData: CsvData): CustomerViewData => {
      const customerVData = new CustomerViewData([], []);
      const keys = Object.keys(new Customer('', '', 0, ''));
      for (let r = 1; r < csvData.rows; r++) {
        const customer = new Customer('', '', 0, '');
        for (let c = 0; c < csvData.columns; c++) {
          const index: string =
            String.fromCharCode('A'.charCodeAt(0) + c) + (r + 1);
          if (csvData.contentArray[index]) {
            customer[keys[c]] = csvData.contentArray[index].v;
          } else {
            customer[keys[c]] = '';
          }
        }
        if (isValidCustomerData(customer)) {
          customerVData.customerList.push(customer);
        } else {
          customerVData.invalidCustomerList.push(customer);
        }
      }
      return customerVData;
};

const  isValidCustomerData = (customer: Customer): boolean => {
  return !(
    isNaN(customer.issueCount) ||
    isNaN(Date.parse(customer.dateOfBirth)) ||
    customer.dateOfBirth === ''
  );
};
