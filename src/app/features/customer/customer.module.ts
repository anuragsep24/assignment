import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from 'src/app/shared/shared.module';
import { CustomerManageComponent } from './manage/manage.component';

@NgModule({
  declarations: [
    CustomerManageComponent,
  ],
  exports: [
    CustomerManageComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    SharedModule,
  ],
})
export class CustomerModule { }
