import { Customer } from './customer.util';

describe('Service: Auth', () => {
  let customer: Customer;
  beforeEach(() => {
    customer = new Customer('John', 'Doe', 1, '323424');
  });

  it(`should accept csv file and read it`, () => {
    expect(customer).toBeTruthy();
  });

  afterEach(() => {
    customer = null;
  });
});
